#!/usr/bin/python3

# ==============================================================================
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# 
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <https://www.gnu.org/licenses/>.
# ==============================================================================

# This file is a modification of a template for command-line programs. The
# following packages are used for boilerplate code -- handling arguments and
# logging.

# The program consists of three parts:
#
# 1. Prelude, where we import the relevant packages and define boilerplate,
#    argument- and log-handling code.
#
# 2. First Act, where there is step-function `derivative` definition. There's
#    all the *dynamical* code, so-to-say.
#
# 3. Second Act, where we
#
#    - Handle arguments and set-up the logs
#
#    - Define the starting date for the simulation
#
#    - Calculate the solution
#
#    - Plot solution

# Prelude ======================================================================
import argparse
import logging
from termcolor import colored

# These packages, on the other hand, are used for the specific purpose of the
# program: simulating ODEs.
from scipy.integrate import solve_ivp
import numpy as np
import matplotlib.pyplot as plt

# This is the name of the program, to be showed on the command line.
NAME = 'FishSim'
DESCRIPTION = "A fish simulator using some differential equations."

# This is code that handles log output formats. Ignore it if you are looking for
# the simulation code.
class CustomFormatter(logging.Formatter):
    """Logging Formatter to add colors and count warning / errors
    Adapted from https://stackoverflow.com/a/56944256/11672072"""

    # format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"
    format = "%(levelname)s: %(message)s"

    FORMATS = {
        logging.DEBUG:    colored(format, 'grey'),
        logging.INFO:     colored(format, 'white'),
        logging.WARNING:  colored(format, 'yellow'),
        logging.ERROR:    colored(format, 'red'),
        logging.CRITICAL: colored(format, 'red', attrs=['bold'])
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)
# ==============================================================================

# First Act ====================================================================
def plastic(t):
    '''Amount of plastic over time'''
    return t*0.05

def derivatives(t, y):
    '''The step function for the system, fed into `scipy.integrate.solve_ivp`
    Given a state `y`, it returns the derivative at that point for every
    variable. See the documentation for the `scipy.integrate.solve_ivp` at
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html'''

    # Extract state variables from state vector `y`
    za = y[0] # Fish in ambient waters
    fa = y[1] # Zooplankton in ambient waters
    fs = y[2] # Fish in slicks
    zs = y[3] # Zooplankton in slicks

    # CUSTOMIZATION ============================================================
    
    zooplankton_reproductive_rate = 0.38 # Known
    fish_death_rate               = 0.21 # Known
    predation_rate                = 0.52 # Known

    conversion_rate               = 0.5 # Reasonable
    zooplankton_carrying_capacity = 1.0

    zooplankton_flow              = 0.05
    scavenging_rate               = 0.2
    fish_flow                     = 0.0
    
    a  = zooplankton_reproductive_rate
    b  = zooplankton_reproductive_rate/zooplankton_carrying_capacity
    c  = zooplankton_flow
    df = conversion_rate*predation_rate
    dz = predation_rate
    e  = scavenging_rate
    f  = fish_death_rate
    g  = fish_flow

          # Growth  Logistic    Predation  Flow   Scavenging
    dza =   a*za    - b*(za*za) - dz*fa*za - c*za
    dfa = - f*fa                + df*fa*za - g*fa - e*(fa-fs)*(zs-za)
    dfs = - f*fs                + df*fs*zs + g*fa - e*(fs-fa)*(za-zs)
    dzs =   a*zs    - b*(zs*zs) - dz*fs*zs + c*za

    # ==========================================================================
    
    return [dza, dfa, dfs, dzs]
# ==============================================================================

# Second Act ===================================================================

if __name__ == '__main__':
    # Argument handling
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('-v', '--verbose',
            action = 'store_true',
            help = 'set verbose mode')
    parser.add_argument('-p', '--phase-space',
            action = 'store_true',
            help = 'plot phase space instead of time plot')
    args = parser.parse_args()

    # Set-up logger object
    logger = logging.getLogger(__name__)
    handler = logging.StreamHandler()
    handler.setFormatter(CustomFormatter())
    logger.addHandler(handler)
    
    # Set verbose level
    if args.verbose:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)

    # Set-up initial state as a state vector
    initial_conditions = [
                0.5, # Zooplankton in the ambient
                0.2, # Fish in the ambient
                0.0, # Fish in the slicks
                0.0  # Zooplankton in the slicks
            ]

    # Define the simulation time interval as a 2-tuple
    SIMULATION_TIME = 50
    interval = (0.0, SIMULATION_TIME)
   
    # Calculate solution
    # For details, see the documentation for the `scipy.integrate.solve_ivp` at
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html
    logger.info('Starting calculation of solution...')
    sol = solve_ivp(
                derivatives,
                interval,
                initial_conditions,
                dense_output=True
            )
    t = np.linspace(0, SIMULATION_TIME, 3000) # The time points at which to calculate solution
    z = sol.sol(t)
    logger.info('Calculation done!')
    
    # Plot the solution
    logger.info('Plotting the solution...')

    if args.phase_space:
        plt.plot(z[0], z[1])
        plt.xlabel('Fish')
        plt.ylabel('Zooplankton')
    else:
        plt.plot(t, z.transpose())
        plt.legend(
                [
                    'Zooplankton in ambient',
                    'Fish in ambient',
                    'Fish in slicks',
                    'Zooplankton in slicks'
                ]
            )
    
    plt.title('Fish-Zooplankton Dynamics')

    plt.show()

